﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;
using CumSlimes;

namespace CumSlimesArePeopleToo
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony harmony = new Harmony("NCS_H");
            harmony.Unpatch(AccessTools.Method(typeof(TaleUtility), "Notify_PawnDied"), AccessTools.Method(typeof(Patch_TaleUtility), "SkipPawnKilledIfCumSlime"));
            harmony.Unpatch(AccessTools.Method(typeof(PawnDiedOrDownedThoughtsUtility), "GetThoughts"), AccessTools.Method(typeof(Patch_PawnDiedOrDownedThoughtsUtility), "SkipPawnThoughtsIfCumSlime"));
        }
    }
}
